import React from 'react';

class NewAppt extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            vin: "",
            customerName: "",
            date: "",
            technicians: [],
            technician: "",
            reason: "",
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }
    handleChange(event) {
        this.setState({ [event.target.name]: event.target.value });
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        data.customer_name = data.customerName
        delete data.customerName;
        delete data.technicians;
        const url = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const appt = await response.json();
            console.log(appt);
            const cleared = {
                vin: '',
                customerName: '',
                date: '',
                technician: '',
                reason: '',
            };
            this.setState(cleared);
        }
    }

    async componentDidMount() {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log(data);
            this.setState({ technicians: data.technicians });
            console.log(data.technicians);
        }
    }
 
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create an Appointment</h1>
                        <form onSubmit={this.handleSubmit} id="create-appointment-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.vin} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control" />
                                <label htmlFor="name">VIN</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.customerName} placeholder="Customer Name" required type="text" name="customerName" id="customer_name" className="form-control" />
                                <label htmlFor="room_count">Customer Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.date} placeholder="Date" required type="date" name="date" id="date" className="form-control" />
                                <label htmlFor="room_count">Date</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleChange} value={this.state.technician} required name="technician" id="technician" className="form-select">
                                    <option value="">Select a Technician</option>
                                    {this.state.technicians.map(technician => {
                                        return (
                                            <option key={technician.id} value={technician.id}>
                                                {technician.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
                                <label htmlFor="room_count">Reason</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }



}

export default NewAppt;