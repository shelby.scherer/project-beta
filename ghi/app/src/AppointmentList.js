import React, { useState, useEffect } from 'react'

const AppointmentList = () => {
    const [appointments, setAppt] = useState([])


    const getApt = async () => {
        const url = 'http://localhost:8080/api/appointments/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setAppt(data);
        }
    }

    const deleteApt = async (id) => {
        const url = (`http://localhost:8080/api/appointments/${id}/`)
        console.log(url)
        const fetchConfig = {
            method: "delete",
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            getApt();
            const data = await response.json()
            console.log(data)
        }
    }

    const finished = async (id) => {
        const url1 = (`http://localhost:8080/api/appointments/${id}/`);
        console.log(url1)
        const fetchConfig = {
            method: "put",
            body: JSON.stringify({ completed: true }),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(url1, fetchConfig);
        if (response.ok) {
            getApt();
            const data = await response.json();
            console.log(data);
            window.location.reload();
        }
    }


    useEffect(() => {
        getApt()
    }, [])


    return (
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">VIN</th>
                        <th scope="col">Customer Name</th>
                        <th scope="col">Date</th>
                        <th scope="col">Technician</th>
                        <th scope="col">Reason</th>
                        <th scope="col">VIP</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.appointments?.map(appt => {
                        if (appt.completed == false) {
                            if (appt.vip == true) {
                                return (
                                    <tr key={appt.id}>
                                        <td>{appt.vin}</td>
                                        <td>{appt.customer_name}</td>
                                        <td>{appt.date}</td>
                                        <td>{appt.technician.name}</td>
                                        <td>{appt.reason}</td>
                                        <td>VIP customer</td>
                                        <td>
                                            <button type="button" className="btn btn-danger" onClick={() => deleteApt(appt.id)}>Cancel</button>
                                            <button type="button" className="btn btn-success" onClick={() => finished(appt.id)}>Finished</button>
                                        </td>
                                    </tr>
                                )
                            } else {
                                return (
                                    <tr key={appt.id}>
                                        <td>{appt.vin}</td>
                                        <td>{appt.customer_name}</td>
                                        <td>{appt.date}</td>
                                        <td>{appt.technician.name}</td>
                                        <td>{appt.reason}</td>
                                        <td>Regular customer</td>
                                        <td>
                                            <button type="button" className="btn btn-danger" onClick={() => deleteApt(appt.id)}>Cancel</button>
                                            <button type="button" className="btn btn-success" onClick={() => finished(appt.id)}>Finished</button></td>
                                    </tr>
                                )}
                            }
                        }
                    )
                }
                </tbody>
            </table>
        </div>
    )

}

export default AppointmentList;