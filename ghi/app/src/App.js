import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalesRecordForm from './SaleRecordForm';
import CustomerForm from './CustomerForm';
import SalespersonForm from './SalespersonForm';
import SalespersonHistory from './SalespersonHistory';
import NewManufacturerForm from './NewManufacturerForm';
import ManufacturerList from './ListManufacturer';
import NewModelForm from './NewVehicleModelForm';
import ListVehicleModels from './ListVehicleModels';
import AllSales from './AllSales';
import AppointmentList from './AppointmentList';
import NewAppt from './NewAppt';
import TechForm from './TechForm';
import ServiceHistory from './ServiceHistory';
import AutoList from './AutoList';
import AutoNew from './AutoNew';



function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/new_customer" element={<CustomerForm />} />  
          <Route path="/new_salesperson" element={<SalespersonForm />} />
          <Route path="/new_sale" element={<SalesRecordForm />} />
          <Route path="/salesperson_history" element={<SalespersonHistory />} />
          <Route path="/new_manufacturer" element={<NewManufacturerForm />} />
          <Route path="/manufacturer_list" element={<ManufacturerList />} />
          <Route path="/new_model" element={<NewModelForm />} />
          <Route path="/model_list" element={<ListVehicleModels />} />
          <Route path="/all_sales" element={<AllSales />} />

        <Route path="/" element={<MainPage />} />
        <Route path="/appointments" element={<AppointmentList />} />
        <Route path="/appointments/new" element={<NewAppt />} />
        <Route path="/appointments/all" element={<ServiceHistory />} />
        <Route path="/technicians/new" element={<TechForm />} />
        <Route path="/autos/all" element={<AutoList />} />
        <Route path="/autos/new" element={<AutoNew />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
