import React from 'react';

class ServiceHistory extends React.Component {
    constructor(props) {
        super(props)
        this.state = {

            vin: "",
            appointments: [],
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }
    handleChange(event) {
        this.setState({ [event.target.name]: event.target.value });
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.appointments;
        const url = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "get",
            // body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {

            const appt = await response.json();

            this.state.appointments = appt.appointments;

            const apt = appt.appointments.filter(o => {
                return o.vin == this.state.vin
            })
            console.log(apt);

            // this.state.appointments = appt.appointments

            //  if (this.state.appointments.vin in this.state.appointments){
            //     return this.state.vin

            //  }
            //  console.log(this.state.appointments.vin)



            //  console.log(appt);


            const cleared = {
                // vin: '',
                appointment: '',
            };
            this.setState(cleared);
        }
    }

    // async componentDidMount() {
    //     const url = 'http://localhost:8080/api/technicians/';
    //     const response = await fetch(url);

    //     if (response.ok) {
    //         const data = await response.json();
    //         console.log(data);

    //     }
    // }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>History Search</h1>
                        <form onSubmit={this.handleSubmit} id="appointment-history-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.vin} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control" />
                                <label htmlFor="name">VIN</label>
                            </div>
                            <button className="btn btn-primary">Search VIN records</button>
                        </form>
                    </div>
                </div>
                <div>
                    <h3>Welcome to</h3>
                </div>
                {/* <header> */}
                <h2>Appointment History</h2>
                {/* </header> */}
                <table className="table table-striped">

                    <thead>
                        <tr>
                            <th scope="col">VIN</th>
                            <th scope="col">Customer Name</th>
                            <th scope="col">Date</th>
                            <th scope="col">Technician</th>
                            <th scope="col">Reason</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.appointments.filter(o =>
                            o.vin == this.state.vin).map(o => {
                            return (
                                <tr key={o.id}>

                                    <td>{o.vin}</td>
                                    <td>{o.customer_name}</td>
                                    <td>{o.date}</td>
                                    <td>{o.technician.name}</td>
                                    <td>{o.reason}</td>

                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        );
    }



}

export default ServiceHistory;


