import React, { useState, useEffect } from 'react'

const AutoList = () => {
    const [autos, setAuto] = useState([])


    const getAuto = async () => {
        const url = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setAuto(data);
        }
    }

    useEffect(() => {
        getAuto()
    }, [])


    return (
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Make</th>
                        <th scope="col">Model</th>
                        <th scope="col">Year</th>
                        <th scope="col">Color</th>
                        <th scope="col">VIN</th>
                    </tr>
                </thead>
                <tbody>
                    {autos.autos?.map(auto => {
                        
                            return (
                                <tr key={auto.id}>
                                    <td>{auto.model.manufacturer.name}</td>
                                    <td>{auto.model.name}</td>
                                    <td>{auto.year}</td>
                                    <td>{auto.color}</td>
                                    <td>{auto.vin}</td>
                                </tr>
                            )
                    })}
                </tbody>
            </table>
        </div>
    )

}

export default AutoList;