import React from 'react';

class SaleRecordForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            automobiles: [],
            salespeople: [],
            customers: [],
            price: '',
        };

        this.handleAutomobileChange = this.handleAutomobileChange.bind(this);
        this.handleSalespersonChange = this.handleSalespersonChange.bind(this);
        this.handleCustomerChange = this.handleCustomerChange.bind(this);
        this.handlePriceChange = this.handlePriceChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        console.log(data)
        delete data.automobiles;
        delete data.salespeople;
        delete data.customers;

        const saleUrl = `http://localhost:8090/api/sales/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(saleUrl, fetchConfig);
        console.log(response)
        if (response.ok) {
            const new_sale = await response.json();

            const cleared = {
                salespeople: [],
                automobiles: [],
                customers: [],
                price: '',
            };
            this.setState(cleared);
        }
    }

    handleAutomobileChange(event) {
        const value = event.target.value;
        this.setState({ automobile: value })
    }

    handleSalespersonChange(event) {
        const value = event.target.value;
        this.setState({ salesperson: value })
    }

    handleCustomerChange(event) {
        const value = event.target.value;
        this.setState({ customer: value })
    }

    handlePriceChange(event) {
        const value = event.target.value;
        this.setState({ price: value })
    }

    async componentDidMount() {
    
        const automobileUrl = 'http://localhost:8100/api/automobiles/';
        const automobileResponse = await fetch(automobileUrl);
        const salespersonUrl = 'http://localhost:8090/api/new_salesperson/';
        const salespersonResponse = await fetch(salespersonUrl);
        const customerUrl = 'http://localhost:8090/api/new_customer/';
        const customerResponse = await fetch(customerUrl);

        if (automobileResponse.ok && salespersonResponse.ok && customerResponse.ok) {

            const automobileData = await automobileResponse.json();
            const salespersonData = await salespersonResponse.json();
            const customerData = await customerResponse.json();

            this.setState({ automobiles: automobileData.autos })
            this.setState({ salespeople: salespersonData.sales_people})
            this.setState({ customers: customerData.customers })
        }
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>New Sale Record</h1>
                        <form onSubmit={this.handleSubmit} id="create-record-form">

                            <div className="mb-3">
                                <select onChange={this.handleAutomobileChange} value={this.state.automobile} required id="automobile" name="automobile" className="form-select">
                                    <option value="">Choose an Automobile</option>
                                    {this.state.automobiles.map(automobile => {
                                        return (
                                            <option key={automobile.vin} value={automobile.vin}>{automobile.vin}</option>
                                        );
                                    })}
                                </select>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleSalespersonChange} value={this.state.salesperson} required id="salesperson" name="salesperson" className="form-select">
                                    <option value="">Choose a sales person</option>
                                    {this.state.salespeople.map(salesperson => {
                                        return (
                                            <option key={salesperson.employeeNumber} value={salesperson.employeeNumber}>{salesperson.name}</option>
                                        );
                                    })}
                                </select>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleCustomerChange} value={this.state.customer} required id="customer" name="customer" className="form-select">
                                    <option value="">Choose a customer</option>
                                    {this.state.customers.map(customer => {
                                        return (
                                            <option key={customer.id} value={customer.id}>{customer.name}</option>
                                        );
                                    })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handlePriceChange} value={this.state.price} placeholder="Price" required type="number" max="99999999.99" step="1" name="price" id="price" className="form-control" />
                                <label htmlFor="price">Sale price</label>
                            </div>
                            <button className="btn btn-primary" id="newSaleBtn">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default SaleRecordForm;


