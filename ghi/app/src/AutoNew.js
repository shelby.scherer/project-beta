import React from 'react';

class AutoNew extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            // make: "",
            models: [],
            // model: "",
            year: "",
            color: "",
            vin: "",
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleModel = this.handleModel.bind(this);

    }
    handleChange(event) {
        this.setState({ [event.target.name]: event.target.value });
    }
    handleModel(event) {
        const value = event.target.value;
        this.setState({ model_id: value });
    }


    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.models;
        const aUrl = `http://localhost:8100/api/automobiles/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(aUrl, fetchConfig);
        if (response.ok) {
            const autoN = await response.json();
            console.log(autoN);
            const cleared = {
                // make: "",
                model: "",
                year: "",
                color: "",
                vin: "",
            };
            this.setState(cleared);
        }
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log(data);
            this.setState({ models: data.models });
            console.log(data.models);
        }
    }






    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Register an Automobile</h1>
                        <form onSubmit={this.handleSubmit} id="create-automobile-form">
                            {/* <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.make} placeholder="Make" required type="text" name="make" id="make" className="form-control" />
                                <label htmlFor="name">Make</label>
                            </div> */}
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.year} placeholder="Year" required type="year" name="year" id="year" className="form-control" />
                                <label htmlFor="room_count">Year</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                                <label htmlFor="room_count">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.vin} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control" />
                                <label htmlFor="room_count">VIN</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleModel} value={this.state.model} required name="model" id="model" className="form-select">
                                    <option value="">Model</option>
                                    {this.state.models.map(model => {
                                        return (
                                            <option key={model.id} value={model.id}>
                                                {model.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }



}

export default AutoNew;