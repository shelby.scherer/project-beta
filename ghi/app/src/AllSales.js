// import React from 'react';

// function SaleColumn(props) {
//     return (
//         <div className="col">
//             {props.list.map(sale => {
//                 console.log(props)
//                 return (
//                     <div key={sale.id} className="card mb-3 shadow">
//                         <div className="card-body">
//                             <ul className="list-unstyled">
//                                 <h4 className="card-title">Sale {sale.id}:</h4>
//                                 <ul>
//                                     <li>Salesperson:<p id="liData">{sale.salesperson.name}</p></li>
//                                     <li>Employee Number:<p id="liData">{sale.salesperson.employeeNumber}</p></li>
//                                     <li>Customer:<p id="liData">{sale.customer.name}</p></li>
//                                     <li>VIN:<p id="liData">{sale.automobile.vin}</p></li>
//                                     <li>Price:<p id="liData">{sale.price}</p></li>
//                                 </ul>
//                             </ul>
//                         </div>
//                     </div>
//                 );
//             })}
//         </div>
//     );
// }


// class AllSales extends React.Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             saleColumn: [[],[],[]],
//         }
//     }
//     async componentDidMount() {
//         const url = "http://localhost:8090/api/sales/";

//         try {
//             const response = await fetch(url);
//             if (response.ok) {
//                 const data = await response.json();

//                 const requests = [];
//                 for (let sale of data.sales) {
//                     const detailUrl = "http://localhost:8090/api/sales/${sale.id}/";
//                     requests.push(fetch(detailUrl));
//                 }

//                 const responses = await Promise.all(requests);

//                 const saleColumns = [[], [], []];


//                 let i = 0;
//                 for (const saleResponse of responses){
//                     if (saleResponse.ok){
//                         const details = await saleResponse.json();
//                         saleColumns[i].push(details);
//                         i = i +1;
//                         if (i>2){
//                             i=0;
//                         }
//                     }else{console.error(saleResponse);
//                     }
//                 }
//                 this.setState({ saleColumns: saleColumns});
//             }
//         }catch(e){
//             console.error(e)
//         }
        
//     }
//     render() {
//         return(
//             <>
//                 <div className="px-4 py-5 my-5 mt-0 text-center">
//                     <img className="bg-white rounded shadow d-block mx-auto mb-4" alt="" width="600"/>
//                     <h1 className="display-5 fw-bold"> All Sales </h1>
//                 </div>
//                 <div className="container" id="salesContainer">
//                     <h3 id="salesScroll"></h3>
//                     <div className="row">
//                         {this.state.saleColumn.map((saleList, index) =>{
//                             return (
//                                 <SaleColumn key={index} list={saleList} />
//                             )
//                         })}
//                     </div>
//                 </div>
//             </>
//         )
//     }
// }

// export default AllSales;

import React from 'react';

class AllSales extends React.Component {
    constructor(props) {
        super(props)
        this.state = {sales: []}
    }
    async componentDidMount() {
        const url = "http://localhost:8100/api/sales/";

        const response = await fetch(url);
        
        if (response.ok) {
            const data = await response.json();
            let sales = data
            this.setState({sales : data.sales})
        }
    }

    render() {
        return (
            <div className="row">
                <div className="offset-4 col-8">
                    <div className="shadow p-3 mt-4">
                    <h1>Sales</h1>
                    <table className="table">
                        <thead>
                            <tr>
                                <th>Salesperson Name</th>
                                <th>emplyee number</th>
                                <th>Customer Name</th>
                                <th>Vin number</th>
                                

                            </tr>
                        </thead>
                        <tbody>
                            {this.state.sales.map(sale => {
                                return (
                                    <tr key={ sale.id }>
                                    <td>{ sale.salesperson.name }</td>
                                    <td>{ sale.customer.name }</td>

                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        )
    }
}

export default AllSales;