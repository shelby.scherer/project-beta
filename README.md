# CarCar

Team:

* Israel Navarrete - Sales
* Murad Khudiev - Service 




## Design


To create the application on your personal computer, you need to first fork and clone the project using 'git clone https://gitlab.com/i.navarrete/project-beta'. CD into the cloned project directory and enter 'code .' command to open the project in VS Code. You will need to have your docker desktop installed and opened on your computer as well.
 
In your terminal run the following commands:
 
```
docker volume rm beta-data
Run docker volume create beta-data
Run docker-compose build
Run docker-compose up
 
```
 
That will create the database in volumes as well as the images and containers in docker for running the application. The backend of the application runs on ports 8100 for the inventory microservice, on port 8090 for the sales microservice and on port 8080 for the service microservice. The inventory serves as the aggregate as we draw certain models from the inventory into other microservices and pull data from it.
 
The frontend of the application is built using React.js and is running on port 3000. You should now have your application running if you go on localhost:3000 in your browser.
 

## Application Diagram

https://www.notion.so/Diagram-52a564a821fb4bf3aa8069b83d86d68b

or 

/project-beta/ghi/app/public/Screen Shot 2022-12-12 at 4.09.33 PM.png but this one does not seem to show the diagram on my end.

## Service microservice

After we are done cloning our project and are already in its directory and opened with VS code, we need to register our microservice in the settings file for the service, we add 'service_rest.apps.ServiceRestConfig' to installed apps block in the settings file.

- We then start creating models for the Service microservice.

The models created and their properties:

Automobile: vin (Automobile is Value Object)

Technician: name, employee_number

Appointment: vin, customer_name, date, technician, reason, completed, vip

- The models we created must be registered in the service_rest.admin file

Go inside the terminal of the service-api-1 running container and run the following commands:

python manage.py makemigrations
python manage.py migrate

also run the migrations in the inventory-api-1 container, the models for inventory were created in advance.

- The next step is to open the views file and import models we just created into it after which we need to create encoders and views for the models listed above.

The view functions I created for the models are:

api_list_technicians
api_list_appointments
api_show_appointments
api_show_technicians

The views above were created using the following encoders to pass in the data of different formats:

TechEncoder
AutoEncoder
ApptEncoder    
ApptDetEncoder

The views created allow the user to see a list, create, update, delete or show a specific object for the models that we have.

- We need to create a file called urls.py in the service_rest folder and in there we need to write the urls paths to each function of the views we created.

``
urlpatterns = [
    path("technicians/", api_list_technicians, name="api_list_technicians"),
    path("technicians/<id>/", api_show_technicians, name="api_show_technicians"),
    path("appointments/", api_list_appointments, name="api_list_appointments"),
    path("appointments/<id>/", api_show_appointments, name="api_show_appointments"),
]
``

We also need to include those paths in the service project urls file by adding "path('api/', include('service_rest.urls'))" to url patterns which will pass all the urls we just created to the project.

Have your insomnia open and create GET, POST, PUT and DELETE requests for Technicians and Appointments, I also did them for Automobiles and Vehicle Models although that is not necessary since you can create Autos and Models in your browser. Run the following command in the inventory-api-1 container 'python manage.py createsuperuser' and then log into localhost:8100 page in your browser, that should let you into the menu to create a new model or automobile or manufacturer.

TECHNICIANS:

- List all technicians: 

    METHOD = GET;
    http://localhost:8080/api/technicians/

- Show a technician: 

    METHOD = GET;
    http://localhost:8080/api/technicians/<id>/


- Create a technician: 

    METHOD = POST;

    Make sure to have the body set to JSON and use headers:

    {
    "name": "QWERT",
    "employee_number": 3
    }

    http://localhost:8080/api/technicians/;



- Update a technician: 

    METHOD = PUT;

    Make sure to have the body set to JSON and use headers:

    {
    "name": "Updated Name for the technician",
    "employee_number": 7
    }

    http://localhost:8080/api/technicians/<id>/;



- Delete a technician: 

    METHOD = DELETE;
    http://localhost:8080/api/technicians/<id>/



APPOINTMENTS:

- List all appointments: 

    METHOD = GET;
    http://localhost:8080/api/appointments/

- Show an appointment: 

    METHOD = GET;
    http://localhost:8080/api/appointments/<id>/


- Create an appointment: 

    METHOD = POST;

    Make sure to have the body set to JSON and use headers:

    {
	"vin": "098CC5FB2AN120174",
    "customer_name": "AnthJKHJHHJHJGGHJGHJony",
	"technician": 3,
	"date": "2023-11-03",
	"reason": "Oil leakage"
    }

    http://localhost:8080/api/appointments/;



- Update an appointment: 

    METHOD = PUT;

    Make sure to have the body set to JSON and use headers:

    {	
    "reason": "HJVHJHJJHGGJKJKGJKG"
    }

    http://localhost:8080/api/appointments/<id>/;



- Delete an appointment: 

    METHOD = DELETE;
    http://localhost:8080/api/appointments/<id>/




The backend and its functionality portion for the project are done. The frontend portion was done through React and you can access the application on http://localhost:3000/. The Navbar should include all the components you need to create, list, delete and see history for appointments as well as create a technician for the Service microservice. It should also include pages for Automobiles, Vehicle Models and Manufacturers and have the ability to create them and add them to the existing database and list all the current objects.

## Sales microservice



## API Documentation

Document the endpoints of your API for each of the methods you implement (GET, POST, etc..)
Provide sample success responses and sample request body data for the post requests.

You could theoretically screenshot insomnia.


## Value Objects

I had my value object in the diagram but it is on the Notion link I have mentioned here. As stated when listing the models for the service microservice, the value object is Automobiles as we need it to commit any sort of action or function with the app we are running as all our models rely on Automobile.



How to run this application
Open your terminal and cd into the directory of where you would like this program to be and clone the project by typing
git clone https://gitlab.com/i.navarrete/project-beta.git into your terminal. Then cd into the newly cloned project and type in
docker volume create beta-data
docker-compose build
docker-compose up
to get the project running and to view it go to http://localhost:3000 in you browser

Design

Application Diagram
Put image or link to application diagram here. Identify your VOs here in the diagram.
https://excalidraw.com/#room=f9d1696cb4f4d6d59bca,JTrrR8E3W9om0sxh4Ds08Q

Sales microservice
The Sales service allows you to keep to kepp track of automobile sales that come from the inventory.
It also allows you to keep track of the customer, the sales person, and sales records.
After first creating the models, views, and urls for sales. Once that was done I began creating the js files for the front end which were the
Salesrecordform.js, allsales.js, customerform.js,salespersonform.js,saleshistory.js.

API Documentation
Document the endpoints of your API for each of the methods you implement (GET, POST, etc..)
Provide sample success responses and sample request body data for the post requests.
You could theoretically screenshot insomnia.
Create salesperson          GET,POST    path("new_salesperson/", api_new_salesperson, name="api_new_salesperson"),
Get specific sales person   GET         path("salesperson_details/int:pk/", api_salesperson_details, name="api_salesperson_details"),
Get a salespersons records  GET         path("salesperson_records/int:pk/", api_salesperson_records, name="api_salesperson_records"),
Create a new customer       GET,POST    path("new_customer/", api_new_customer, name="api_new_customer"),
Get Customer details        GET         path("customer_details/int:pk/", api_customer_details, name="api_customer_details"),
Create sales                GET,POST    path("sales/", api_sales, name="api_sales"),
Get all sales               GET         path("sales/int:pk/", api_sale_details, name="api_sale_details"),
List manufacturers	            GET	    http://localhost:8100/api/manufacturers/
Create a manufacturer	        POST	http://localhost:8100/api/manufacturers/
Get a specific manufacturer	    GET	    http://localhost:8100/api/manufacturers/:id/
Update a specific manufacturer	PUT	    http://localhost:8100/api/manufacturers/:id/
Delete a specific manufacturer	DELETE	http://localhost:8100/api/manufacturers/:id/
RESTful API endpoints: Vehicle Models
List vehicle models	            GET	    http://localhost:8100/api/models/
Create a vehicle model	        POST	http://localhost:8100/api/models/
Get a specific vehicle model	GET	    http://localhost:8100/api/models/:id/
Update a specific vehicle model	PUT	    http://localhost:8100/api/models/:id/
Delete a specific vehicle model	DELETE	http://localhost:8100/api/models/:id/
RESTful API endpoints: Automobiles
List automobiles	            GET	    http://localhost:8100/api/automobiles/
Create an automobile	        POST	http://localhost:8100/api/automobiles/
Get a specific automobile	    GET	    http://localhost:8100/api/automobiles/:vin/
Update a specific automobile	PUT	    http://localhost:8100/api/automobiles/:vin/
Delete a specific automobile	DELETE	http://localhost:8100/api/automobiles/:vin/