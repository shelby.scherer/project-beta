from django.contrib import admin
from .models import Automobile, Technician, Appointment
# Register your models here.



@admin.register(Appointment)
class Appointment(admin.ModelAdmin):
    pass

@admin.register(Technician)
class Technician(admin.ModelAdmin):
    pass

@admin.register(Automobile)
class Automobile(admin.ModelAdmin):
    pass