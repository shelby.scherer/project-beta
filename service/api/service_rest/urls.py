from django.urls import path

from .views import (
    api_list_technicians,
    api_list_appointments,
    api_show_appointments,
    api_show_technicians,
    
)


urlpatterns = [
    path("technicians/", api_list_technicians, name="api_list_technicians"),
    path("technicians/<id>/", api_show_technicians, name="api_show_technicians"),
    path("appointments/", api_list_appointments, name="api_list_appointments"),
    path("appointments/<id>/", api_show_appointments, name="api_show_appointments"),
]