from django.shortcuts import render
from .models import Automobile, Technician, Appointment
from common.json import ModelEncoder
from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods
# Create your views here.


class TechEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_number",
        "id",
    ]
    
class AutoEncoder(ModelEncoder):
    model = Automobile
    properties = [
        "vin",
        "id",
    ]
    
class ApptEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "vin",
        "customer_name",
        "date",
        "technician",
        "reason",
        "completed",
        "vip",
        "id",
    ]
    
    encoders = {
        "technician": TechEncoder(),
    }
    
            
            
class ApptDetEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "vin",
        "customer_name",
        "date",
        "technician",
        "reason",
        "completed",
        "vip",
        "id",
    ]
    encoders = {
        "technician": TechEncoder(),
    }
    
    
@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        tech = Technician.objects.all()
        return JsonResponse({"technicians": tech}, encoder=TechEncoder, safe=False)
    else:
        content = json.loads(request.body)
        tech = Technician.objects.create(**content)
        return JsonResponse(tech, encoder=TechEncoder, safe=False)
            
            
@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method =="GET":
        appt = Appointment.objects.all()
        return JsonResponse({"appointments": appt}, encoder=ApptEncoder, safe=False)
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(id=content["technician"])
            content["technician"] = technician
            try:
                if Automobile.objects.get(vin=content["vin"]):
                    content["vip"] = True
            except Automobile.DoesNotExist:
                    content["vip"] = False
        except Exception as e:
            return JsonResponse({"message": str(e)}, status=400)       
        try:
            appt1 = Appointment.objects.create(**content)
            return JsonResponse(appt1, encoder=ApptDetEncoder, safe=False)
        except Exception as e:
            return JsonResponse({"message": str(e)}, status=400)
        
    
@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_appointments(request, id):
    if request.method =="GET":
        try:
            appt = Appointment.objects.get(id=id)
            return JsonResponse(appt, encoder=ApptDetEncoder, safe=False)
        except Exception as e:
            return JsonResponse({"message": str(e)}, status=400)
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            Appointment.objects.filter(id=id).update(**content)
            appt = Appointment.objects.get(id=id)
            return JsonResponse(appt, encoder=ApptDetEncoder, safe=False)
        except Exception as e:
            return JsonResponse({"message": str(e)}, status=400)
    else:
        try:
            count, _ = Appointment.objects.filter(id=id).delete()
            return JsonResponse({'deleted': count>0})
        except Exception as e:
            return JsonResponse({"message": str(e)}, status=400)



@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_technicians(request, id):
    if request.method =="GET":
        try:
            tech = Technician.objects.get(id=id)
            return JsonResponse(tech, encoder=TechEncoder, safe=False)
        except Exception as e:
            return JsonResponse({"message": str(e)}, status=400)
    elif request.method == "DELETE":
        try:
            count, _ = Technician.objects.filter(id=id).delete()
            return JsonResponse({'deleted': count>0})
        except Exception as e:
            return JsonResponse({"message": str(e)}, status=400)
    else:
        try:
            content = json.loads(request.body)
            Technician.objects.filter(id=id).update(**content)
            tech = Technician.objects.get(id=id)
            return JsonResponse(tech, encoder=TechEncoder, safe=False)
        except Exception as e:
            return JsonResponse({"message": str(e)}, status=400)
        