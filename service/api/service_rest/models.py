from django.db import models
from django.urls import reverse
# from .models import Automobile, VehicleModel
# Create your models here.



class Automobile(models.Model):

    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return self.vin
    
    
class Technician(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.PositiveSmallIntegerField(null=True)
    
    def __str__(self):
        return self.name
    
    
class Appointment(models.Model):
    vin = models.CharField(max_length=200, null=True, blank=True)
    customer_name = models.CharField(max_length=200)
    date = models.DateTimeField(null=True)
    technician = models.ForeignKey(Technician, related_name="service", on_delete=models.CASCADE)
    reason = models.TextField()
    completed = models.BooleanField(default=False, null=True, blank=True)
    vip = models.BooleanField(default=False, null=True, blank=True)
    
    def __str__(self):
        return self.vin
    
    